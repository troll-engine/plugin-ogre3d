# EARLY WORK - NOT YET USABLE

<div align="center">
<p align="center"><img src="https://gitlab.com/troll-engine/plugin-ogre3d/raw/master/logo.svg" align="center" width="350" height="250" alt="Project icon"></p>

<blockquote>
<p align="center">Ogre3D plugin for Troll Engine</p>
</blockquote>

<p align="center">
  <a href="https://gitlab.com/troll-engine/plugin-ogre3d/commits/master" alt="pipeline status"><img src="https://gitlab.com/troll-engine/plugin-ogre3d/badges/master/pipeline.svg" /></a>
  <a href="https://gitlab.com/troll-engine/plugin-ogre3d/commits/master" alt="coverage report"><img src="https://gitlab.com/troll-engine/plugin-ogre3d/badges/master/coverage.svg" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/plugin-ogre3d" alt="version"><img src="https://badgen.net/npm/v/@troll-engine/plugin-ogre3d" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/plugin-ogre3d" alt="license"><img src="https://badgen.net/npm/license/@troll-engine/plugin-ogre3d" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/plugin-ogre3d" alt="downloads"><img src="https://badgen.net/npm/dt/@troll-engine/plugin-ogre3d" /></a>
  <a href="https://discord.gg/ewwaSxv" alt="discord"><img src="https://discordapp.com/api/guilds/621750483098271745/widget.png" /></a>
</p>

</div>
</p>

```bash
npm install --save @troll-engine/plugin-ogre3d
```
